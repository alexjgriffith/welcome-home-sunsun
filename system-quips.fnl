(local tiny (require "lib.tiny"))
(local quips-system (tiny.processingSystem))

(tset quips-system :filter (. (require "functions-quips") :filter))

(tset quips-system :process
      (if develop
          (fn [self entities dt]            
            ((. (require "functions-quips") :process) self entities dt))
          (. (require "functions-quips") :process)))

(tset quips-system :name "quips")

quips-system

