(local tiny (require "lib.tiny"))
(local pointer-system (tiny.processingSystem))

(tset pointer-system :filter (. (require "functions-pointer") :filter))

(tset pointer-system :process
      (if develop
          (fn [self entities dt]
            ((. (lume.hotswap "functions-pointer") :process) self entities dt))
          (. (require "functions-pointer") :process)))

(tset pointer-system :name "pointer")

;; (pp pointer-system)
pointer-system


