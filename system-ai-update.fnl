(local tiny (require "lib.tiny"))
(local ai-update-system (tiny.processingSystem))

(tset ai-update-system :filter (. (require "functions-ai-update") :filter))

(tset ai-update-system :process
      (if develop
          (fn [self entities dt]            
            ((. (require "functions-ai-update") :process) self entities dt))
          (. (require "functions-ai-update") :process)))

(tset ai-update-system :name "ai-update")

(tset ai-update-system :active true)
;; (pp ai-update-system)
ai-update-system
