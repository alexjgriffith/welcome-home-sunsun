(local tiny (require "lib.tiny"))
(local utils (require "utils"))
(local editor (tiny.processingSystem))
(tset editor :filter (. (require "functions-editor") :editor :filter))
(tset editor :process
      (utils.process (. (require "functions-editor") :editor :process)))
(tset editor :name "editor")

(local tiny (require "lib.tiny"))
(local utils (require "utils"))
(local save-map (tiny.processingSystem))
(tset save-map :filter (. (require "functions-editor") :save-map :filter))
(tset save-map :process
      (utils.process (. (require "functions-editor") :save-map :process)))
(tset save-map :name "save-map")
(tset save-map :active false)

{:editor editor :save-map save-map}
