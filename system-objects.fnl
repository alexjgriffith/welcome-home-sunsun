(local tiny (require "lib.tiny"))
(local objects-system (tiny.processingSystem))

(tset objects-system :filter (. (require "functions-objects") :filter))

(tset objects-system :process
      (if develop
          (fn [self entities dt]            
            ((. (require "functions-objects") :process) self entities dt))
          (. (require "functions-objects") :process)))

(tset objects-system :name "objects")

(tset objects-system :active true)
;; (pp objects-system)
objects-system
