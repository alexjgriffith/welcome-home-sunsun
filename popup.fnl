(local params (require "params"))
(local gamestate (require "lib.gamestate"))
(local popup {})
(local buttons (require "buttons"))
(local game (require mode))
(var odraw game)
(local visits-text (lume.split (love.filesystem.read "text/visits.txt") "\n~"))

(local events
       {:day1 {:title "Day 1"
               :options ["Sandbox" "Story"];;["Continue"]
               :str "Say hello to your new kitty, Sunsun!\n\nYou have to leave for work at 9:30, so make sure she is well fed! \n\nTrain Sunsun by petting her when she does good and spraying her when she does bad \n\nPress esc for controls"
               :newlines 8}
        :day {:title "<Radio Blares>"
               :options ["Wake Up" "Snooze"]
               :str "*radio* LOOKS LIKE THE COST OF CAT FOOD HAS GONE UP AGAIN TODAY.\n\nTHATS EVERY DAY THIS WEEK?\n\nTHIS ECONOMY, I TELL YOU BOB ..."
              :newlines 6}
        :wake-up {:title "Up and Atom"
                  :str "Guess I'd better get out of bed to feed that cat."
                  :options ["Get Up"]}
        :work {:title "Time for Work"
               :options ["Stay" "Leave"]
               :str "It's 9:30 already, Sunsun!?\n\nThat cost of cat food keeps going up!\n\nGotta work to keep that cat fed."
               :newlines 4}
        :call {:title "Boss Calls"
               :options ["Lie" "Truth"]
               :str "Your boss has called. He is wondering why you didn't show up to work today\n\nDo you tell him the truth, you're at home playing with your cat. Or, do you tell him you're sick?"
               :newlines 4}
        :home {:title "Work"
               :str "It's a grind, but you gotta pay for that cat food."
               :options ["Go Home" "Stay Late"]}
        :home-force {:title "Lights Out"
                     :str "Looks like they are turning the lights out at work. Should probably get home and check on Sunsun. I'm sure she misses me."
                     :options ["Go Home"]}        
        :visit1 {:title "Friends Visit"
                :options ["Continue"]
                :str (. visits-text 1)
                 :newlines 8}
        :visit2 {:title "Friends Visit"
                 :options ["Continue"]
                 :str (. visits-text 2)
                 :newlines 8}
        :visit3 {:title "Friends Visit"
                :options ["Continue"]
                :str (. visits-text 3)
                :newlines 6}
        :sleep {:title "Bed Time"
                :str "It's starting to get late. I should look at hitting the hay. I can spend more time with you tomorrow, Sunsun."
                :options ["Go to Bed" "Stay Up"]}
        :sleep-again
        {:title "Bed Time"
         :str "It's starting to get late. I should look at hitting the hay. Another day working to pay for your D@MN food, Sunsun."
         :options ["Bed Time" "Stay Up"]}        
        :force-sleep {:title "Bed Time"
                :str "I can't keep my eyes open. Maybe I'll just lay down here on the ......."
                      :options ["Pass Out"]}
        :ending-dead {:title "SO HUNGRY"
                      :str "Sunsun has been taken away :(\n\nShe is being taken care of by a nice owner who has time for her.\n\nIt's not the end of the world.\n\nShe can come for a visit and play in sandbox mode!!"
                      :options ["Sandbox" "Credits"]
                      :newlines 8}
        :ending-broke {:title "Gone Broke"
                      :str "You can't afford to feed Sunsun and maintain your apartment. You had better find someone who can..\n\nIt's not the end of the world.\n\nShe can come for a visit and play in sandbox mode!!"
                       :options ["Sandbox" "Credits"]
                       :newlines 6}
        :ending-fired {:title "You're Fired"
                       :str "Your boss is pissed you've missed so many days of work looking after Sunsun. You'd better give her away.\n\nAlternatively, you can quit your job and play with her ALL DAY in sandbox mode!!"
                       :options ["Sandbox" "Credits"]
                       :newlines 6}        
        })

(fn make-popup [event]
  (local [title str options newlines]
         [event.title event.str event.options (or event.newlines 0)])
  (local maxwidth 560)
  (local font font.small-font)
  (local lines  (+ newlines (math.ceil (/ (font:getWidth str) maxwidth))))
  (local vspace   (* (font:getHeight str) lines))
  (local title {:type :title :y 20 :oy -10 :ox 0 :w 400 :h 70 :text title})
  (local text {:type :small-text :y 120 :oy -10 :ox 0 :w 560 :h 70 :text str})
  (if (> (length options) 1)
      (do
          (local option1 {:type :button :y (math.min 600 (+ vspace 140))
                          :oy -10 :ox -150 :w 280 :h 70 :text (. options 1)})
        (local option2 {:type :button :y (math.min 600 (+ vspace 140))
                        :oy -10 :ox 150 :w 280 :h 70 :text (. options 2)})
        [title text option1 option2 ])
      (do (local option {:type :button :y (math.min 600 (+ vspace 140)) :oy -10 :ox 0 :w 400 :h 70 :text (. options 1)})
        [title text option ])
      ))

(var elements [])

(local element-click
       {"Continue"
        (fn []
          (sound.play "page")
          (gamestate.switch (if blackboard.story
                                (require mode)
                                (require "game")) {:sleep false :awake true}))

        "Story"
        (fn []
          (sound.play "page")
          (gamestate.switch (if blackboard.story
                                (require mode)
                                (require "game")) {:sleep false :awake true}))
        "Wake Up"
        (fn []
          (sound.play "page")
          (gamestate.switch (require mode)
                            {:awake true :sleep false :hunger -2 :cat-food-cost 2}))
        
        "Snooze"
        (fn []
          (sound.play "page")
          (gamestate.switch (require mode)
                            {:awake false :sleep true :hunger -2 :cat-food-cost 1
                             :time 2}))

        "Get Up"
        (fn []
          (sound.play "page")
          (gamestate.switch (require mode)
                            {:awake true :sleep false :hunger -2}))

        "Truth"
        (fn []
          (sound.play "page")
          (gamestate.switch (require mode)
                            {:truth true :lie false :call-in-count 1}))
        
        "Lie"
        (fn []
          (sound.play "page")
          (gamestate.switch (require mode)
                            {:truth false :lie true :call-in-count 2}))
                
        "Stay"
        (fn []
          (sound.play "page")
          (gamestate.switch (require mode)
                            {:called-in true :at-home true :at-work false}))
        
        "Leave"
        (fn []
          (sound.play "page")
          ;; goto work
          (gamestate.switch (require mode)
                            {:at-work true :at-home false :called-in false :time 16}))

        "Go Home"
        (fn []
          (sound.play "page")
          (gamestate.switch (require mode)
                            {:at-home true :at-work false :overtime false :cash params.income :hunger -2}))

        "Stay Late"
        (fn []
          (sound.play "page")
          (gamestate.switch (require mode)
                            {:at-home false :at-work true
                             :cash params.overtime :overtime true :hunger -2 :time 4}))

        "Stay Up"
        (fn []
          (sound.play "page")
          (gamestate.switch (require mode) ))
        
        "Go to Bed"
        (fn []
          (sound.play "page")
          (gamestate.switch (require mode)
                            {:up-late false :sleep true :awake false :time 16}))


        "Bed Time"
        (fn []
          (sound.play "page")
          (gamestate.switch (require mode)
                            {:up-late false :sleep true :awake false :time 12}))
        
        "Pass Out"
        (fn []
          (sound.play "page")
          (gamestate.switch (require mode)
                            {:up-late true :sleep true :awake false :time 8}))
        
        "Credits"
        (fn []
          (gamestate.switch (require "credits") (require "game")))

        "Sandbox"
        (fn []
          (tset blackboard :story false)
          (sound.play "page")
          (gamestate.switch (require "game")))
        
        "Controls"
        (fn [])})

(fn popup.draw [self]
  (self.from:draw)
  (love.graphics.setColor params.colours.background)
  (love.graphics.rectangle "fill" 170 0 (- 960 (* 2 170)) 640) 
  (buttons:draw elements))

(fn popup.update [self dt]
  (buttons:update elements element-click))


(fn popup.enter [self from event]
  (tset self :from from)
  (set elements (make-popup (. events event)))
  (buttons:enter))

popup
