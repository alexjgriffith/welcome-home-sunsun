(local params (require "params"))

(local default-quip {:time 1 :fade 0.9 :current 0
                     :v -100 :background-colour [1 1 1 0]
                     :colour params.colours.charcoal :text ""
                     :width 100 :pos {:x 0 :y 0} :transparency 1
                     :quip true :render true :font :ui-money})

(local quips {})

(fn make-quip [input-quip text? pos? params?]
  (local quip (lume.clone input-quip))
  (when params? (each [key value (pairs params?)]
                  (tset quip key value)))
  (when text? (tset quip :text text?))
  (when pos? (tset quip :pos pos?))
  quip)

(fn new-quip [name text params]
  (tset quips name (make-quip default-quip text nil params)))

(new-quip "cost" "$ " {:background [1 1 1 0] :time 1 :oy 32 :colour params.colours.cash-green})

(fn [which text?  pos? params?]
  (local quip (. quips which))
  (if quip
      (make-quip quip text? pos? params?)
      (make-quip default-quip text? pos? params?)))
