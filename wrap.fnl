;; move to lua?
(local gamestate (require "lib.gamestate"))
(local repl (require "lib.stdio"))

(local assets (require "params"))
(global profile false)
(global develop true)
(global sound (require "sound"))
(global blackboard {:cash 20
                    :hunger 3
                    :time 12
                    :call-in-count 0
                    :broken []
                    :cat-food-cost 1
                    :story true
                    :cat-name "Sunsun"
                    :cat-type "assets/Cat Animation"
                    :cat-pronoun-1 "her"
                    :cat-pronoun-2 "she"
                    :cat-pronoun-3 "She"
                    :mouse-down-last-frame false})

(global text-mod (fn [text]
                   (local t1 ((. text :gsub) text "Sunsun" blackboard.cat-name))
                   (local t2 ((. t1 :gsub) t1 "her" blackboard.cat-pronoun-1))
                   (local t3 ((. t2 :gsub) t2 "she" blackboard.cat-pronoun-2))
                   ((. t3 :gsub) t3 "She" blackboard.cat-pronoun-3)))

(global font {})
(set font.ui-money (love.graphics.newFont "assets/avara.otf" 40))
(set font.ui-message (love.graphics.newFont "assets/avara.otf" 30))
(set font.pet (love.graphics.newFont "assets/avara.otf" 30))
(set font.console (love.graphics.newFont "assets/inconsolata.otf"  15))
(set font.title-font (love.graphics.newFont "assets/avara.otf" 80))
(set font.normal-font (love.graphics.newFont "assets/avara.otf" 55))
(set font.small-font (love.graphics.newFont "assets/avara.otf" 30))

(set font.very-large-font (love.graphics.newFont "assets/avara.otf" 150))
(set font.credits-font (love.graphics.newFont "assets/avara.otf" 30))
(set font.font-55 (love.graphics.newFont "assets/avara.otf" 70))
(set font.text-font (love.graphics.newFont "assets/avara.otf" 32))
(set font.final-font (love.graphics.newFont "assets/inconsolata.otf"  30))

(math.randomseed (os.time))

;;(var mode (require "splash"))
(global mode "story")

(fn love.load []
  (when profile
    (set love.profiler (require "lib.profile"))
    (love.profiler.hookall "Lua")
    (love.profiler.start))
  (gamestate.registerEvents)  
  (gamestate.switch (require "menu"))  
  (repl.start)
  ;; (sound.play "bgm")
  )

(fn love.keypressed [key code]
  (gamestate.keypressed key code))

(fn love.keyreleased [key code]
  (gamestate.keyreleased key code))

