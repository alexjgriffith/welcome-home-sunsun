(local params (require "params"))
(local stack (require "stack"))
(local loader (require "loader"))
(local activities (require "activities"))
(local params (require "params"))
;;"assets/Cat Animation"
(local player-animations (loader blackboard.cat-type true))
(local scale 4)
(local player {:player :true
               :cash 50
               :time 12
               :time-string "6:00"
               :ai :true
               :scale {:x scale :y scale}
               :collision {:oy 18 :ox (* 3 4) :w  10 :h 12}
               :center-offset {:x (/ (* scale 32) 2) :y (/ (* scale 32) 2)}}) 

(each [key value (pairs player-animations)]
  (tset player key value))
(tset player :anim "Stand Talk")

(tset player :pos {:x 100 :y 100 :w 32 :h 32 :flipped true})

(tset player :vel {:x 0 :y 0})

(tset player :speed params.cat-speed)

(tset player :jumping false)

(tset player :z player.pos.y)

(tset player :render :true)
(tset player :controler :true)
(tset player :sprite :true)

(tset player :goal-stack (stack))

(tset player :message  "Welcome home Sunsun!")

(tset player :stats activities.stats)

(tset player :bump-world-reference
      {:name :player
       :type :sprite
       :w player.collision.w
       :h player.collision.h
       :x (+ player.pos.x player.collision.ox)
       :y (+ player.pos.y player.collision.oy)
       })

(fn [bump-world]
  (: bump-world.world :add player.bump-world-reference
     player.bump-world-reference.x
     player.bump-world-reference.y
     player.bump-world-reference.w
     player.bump-world-reference.h)
  (table.insert bump-world.collidables player.bump-world-reference)
  player)
