(local params (require "params"))
(local gamestate (require "lib.gamestate"))
(local splash {})
(local buttons (require "buttons"))

(var timer 2)

(local elements
       [{:type :title :y 100 :w 400 :h 60 :text "Welcome Home SunSun"}
        {:type :text :y 300 :w 400 :h 60 :text "game by: alexjgriffith"}
        {:type :text :y 400 :w 400 :h 60 :text "music by: cybersdf"}])

(local element-click
       {"New Game"
        (fn [] (gamestate.switch (require "game")))

        "Credits"
        (fn [])
        
        "Controls"
        (fn [])})

(fn splash.draw [self]
  (love.graphics.setColor params.colours.background)
  (love.graphics.rectangle "fill" 0 0 960 640)
  (buttons:draw elements)
    (when (< timer 0)
    (gamestate.switch (require "menu"))))

(fn splash.update [self dt]
  (set timer (- timer dt)))


(fn splash.enter [self]
  (set timer 2))

splash
