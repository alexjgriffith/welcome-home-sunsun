(local tiny (require "lib.tiny"))
(local params (require "params"))

(local sprites {})
(local tiles {})
(local background {})
(local grid {})
(local colliders {})
(local foreground {})
(local pointer {})
(local ai-debug {})
(local hud {})
(local ui {})
(local quips {})

(tset sprites :filter (tiny.requireAll "render" "z" "pos" "sprite"))
(tset tiles :filter (tiny.requireAll "render" "level"))
(tset background :filter (tiny.requireAll "render" "background" "actual-background"))
(tset grid :filter (tiny.requireAll "render" "grid"))
(tset colliders :filter (tiny.requireAll "render" "bump-world"))
(tset foreground :filter (tiny.requireAll "render" "foreground"))
(tset pointer :filter (tiny.requireAll "render" "pointer"))
(tset ai-debug :filter (tiny.requireAll "render" "ai"))
(tset hud :filter (tiny.requireAll "render" "ai")) ;; update later, now just to show stats
(tset ui :filter (tiny.requireAll "render" "ai"))
(tset quips :filter (tiny.requireAll "render" "pos" "quip"))


(fn sprites.process [self entity camera]
  (when entity.player

     (love.graphics.push)   
     (love.graphics.scale 4 4)     
     (let [animation (. entity.animations entity.anim) ]
       (animation.draw animation entity.image entity.pos.x entity.pos.y))
     ;; (let [pos entity.pos
     ;;       x pos.x
     ;;       y pos.y
     ;;       w pos.w           
     ;;       h pos.h]
     ;;   (love.graphics.rectangle "line" x y w h))
     (love.graphics.pop)
     
     ))

(fn sprites.compare [self e1 e2]
  (> e1.z e2.z))

(fn background.process [self entity camera]
  ;;(local camera self.world.camera)
  (love.graphics.setColor entity.color)
  (love.graphics.rectangle "fill" 0 0 960 640)
  (love.graphics.setColor 1 1 1 1))

(fn tiles.process [self entity camera]
        (love.graphics.push)   
        (love.graphics.scale 4 4) 
        (love.graphics.draw entity.tileset-batch)
        ;;(love.graphics.draw entity.fore-batch)
        (love.graphics.draw entity.object-batch)
        ;; when edit
        (love.graphics.draw entity.hover-batch)
        (love.graphics.pop)
     ;;(pp "hello!")
     )


(fn grid.process [self entity camera]
     (for [i 0 (math.floor (/ 640 32))]
       ;; 640 -> 38 (* 16 (ceiling(/ 900 16.0)))
       ;; 960 -> 
       (for [j 0 (math.floor (/ 960 32))]
         (love.graphics.setColor 0 0 0 1)
         (love.graphics.rectangle "line" (* j 32) (* i 32) 32 32)
         (love.graphics.setColor 1 1 1 1)))
     )


(fn colliders.process [self bump-world camera]
  (love.graphics.push)   
  (love.graphics.scale 4 4)
  (love.graphics.setColor 0 0 0 1)
  (each [_ value (pairs bump-world.collidables)]
    (love.graphics.rectangle "line" value.x value.y value.w value.h))
  (love.graphics.setColor 1 1 1 1)
  (love.graphics.pop)
  )

(fn foreground.process [self foreground camera]
  (love.graphics.push)   
  (love.graphics.scale 4 4)  
  (love.graphics.draw foreground.img 0 0)
  (love.graphics.pop)
  )

(fn pointer.process [self pointer camera]
  (love.graphics.push)
  (local scale 4)
  (love.graphics.scale scale scale)
  (love.graphics.setColor [1 1 1 1])
  (local {:x x :y y} pointer.pos)
  ;;(love.graphics.draw pointer.img x y 0 1 1)
  (let [animation (. pointer.animations  pointer.anim)]
    (animation.draw animation pointer.image (/ (-  x (* scale 16)) scale) (/ (- y ( * scale 16)) scale)))
  (love.graphics.setColor [1 1 1 1])
  ;; (love.graphics.circle "line" (/  x  scale) (/ y scale) pointer.radius)
  (love.graphics.pop)
  )

(fn ai-debug.process [self player camera]
  (local objects self.world.objects)
  (local [px py] [ (+ player.pos.x 16) (+ player.pos.y 30)])  
  (love.graphics.push)   
  (love.graphics.scale 4 4)
  (each [_ object (pairs objects)]
    (local [ox oy] [(* (+ object.x (/ object.w 4)) 16)
                    (* (+ object.y   (/ object.h 2) ) 16)])
    (love.graphics.rectangle "line" (- ox 2) (- oy 2) 4 4))
  (love.graphics.setColor 1 0 0 1)
  (love.graphics.rectangle "line" (- px 2) (- py 2) 4 4)
  (love.graphics.setColor 1 1 1 1)
  (love.graphics.pop))

(fn draw-stat [value label colour y]
  (love.graphics.setColor 0 0 0 1)
  (love.graphics.print (text-mod label) 4 (+ y 2))
  (love.graphics.setColor 1 1 1 1)
  (love.graphics.rectangle "fill" 60 y 200 20)
  (love.graphics.setColor colour)  
  (love.graphics.rectangle "fill" 60 y (* 200 value) 20)
)

(fn hud.process [self player camera]
  (local goal (player.goal-stack:peak))
  (local objects self.world.objects )
  (love.graphics.setColor 0 0 0 1)
  (when goal
    (local obj (. self.world.objects goal.goal.key))
    (when obj
      (love.graphics.print (string.format "Goal is: %s %s"
                                          (. obj :type) goal.action )
                           400 4)))
  (love.graphics.setFont font.console)
  (draw-stat player.stats.hunger.y "Hunger" [1 0 0 1] 4)
  (draw-stat player.stats.boredom.y "Boredom" [1 0 0 1] (+ 24 4))
  (draw-stat player.stats.sleepy.y "Sleepy" [1 0 0 1] (+ 24 24 4))
  )

(fn ui.process [self player camera]
  (local goal (player.goal-stack:peak))
  (local objects self.world.objects )
  (love.graphics.setColor params.colours.charcoal)
  (love.graphics.rectangle "fill" 295 13 500 30)
  (love.graphics.setColor params.colours.blood-red)
  (love.graphics.rectangle "fill" 299 17 (* player.stats.hunger.y 492) 22)
  (love.graphics.setColor params.colours.charcoal)
  (love.graphics.setFont font.ui-message)
  (love.graphics.print "Hunger:" 165 12)
  (love.graphics.setColor params.colours.charcoal)
  (love.graphics.setFont font.ui-money)
  (love.graphics.print (string.format "$ %d" player.cash) 30 595)
  (love.graphics.setFont font.ui-message)
  (local message  player.message)
  (when (> (# message) 0)
    (love.graphics.printf  (text-mod message)  150 600 660  "center"))
  (love.graphics.setFont font.ui-money)
  (love.graphics.printf player.time-string 840 595 120 "center"))


(fn quips.process [self quip camera]
  ;; (pp quip)
  (love.graphics.setFont (. font quip.font))
  (love.graphics.setColor quip.colour)
  (love.graphics.printf (text-mod quip.text) quip.pos.x quip.pos.y quip.width "center")
  (love.graphics.setColor 1 1 1 1)
  )

{:sprites sprites :tiles tiles :background background :grid grid :colliders colliders
 :foreground foreground :pointer pointer :ai-debug ai-debug :hud hud :ui ui :quips quips}
