(local tiny (require "lib.tiny"))
(local utils (require "utils"))
(local draw-functions (require "functions-draw"))

(local sprites (tiny.sortedProcessingSystem))
(tset sprites :filter (. (require "functions-draw") :sprites :filter))
(tset sprites :compare
      (utils.compare (. (require "functions-draw") :sprites :compare)))
(tset sprites :process
      (utils.process (. (require "functions-draw") :sprites :process)))
(tset sprites :name "draw-sprites")
(tset sprites :active false)

(local tiles (tiny.processingSystem))
(tset tiles :filter (. (require "functions-draw") :tiles :filter))
(tset tiles :process
      (utils.process (. (require "functions-draw") :tiles :process)))
(tset tiles :name "draw-tiles")
(tset tiles :active false)

(local background (tiny.processingSystem))
(tset background :filter (. (require "functions-draw") :background :filter))
(tset background :process
      (utils.process (. (require "functions-draw") :background :process)))
(tset background :name "draw-background")
(tset background :active false)

(local grid (tiny.processingSystem))
(tset grid :filter (. (require "functions-draw") :grid :filter))
(tset grid :process
      (utils.process (. (require "functions-draw") :grid :process)))
(tset grid :name "draw-grid")
(tset grid :active false)


(local colliders (tiny.processingSystem))
(tset colliders :filter (. (require "functions-draw") :colliders :filter))
(tset colliders :process
      (utils.process (. (require "functions-draw") :colliders :process)))
(tset colliders :name "draw-colliders")
(tset colliders :active false)

(local foreground (tiny.processingSystem))
(tset foreground :filter (. (require "functions-draw") :foreground :filter))
(tset foreground :process
      (utils.process (. (require "functions-draw") :foreground :process)))
(tset foreground :name "draw-foreground")
(tset colliders :active false)


(local pointer (tiny.processingSystem))
(tset pointer :filter (. (require "functions-draw") :pointer :filter))
(tset pointer :process
      (utils.process (. (require "functions-draw") :pointer :process)))
(tset pointer :name "draw-pointer")
(tset colliders :active false)


(local ai-debug (tiny.processingSystem))
(tset ai-debug :filter (. (require "functions-draw") :ai-debug :filter))
(tset ai-debug :process
      (utils.process (. (require "functions-draw") :ai-debug :process)))
(tset ai-debug :name "draw-ai-debug")
(tset colliders :active false)

(local hud (tiny.processingSystem))
(tset hud :filter (. (require "functions-draw") :hud :filter))
(tset hud :process
      (utils.process (. (require "functions-draw") :hud :process)))
(tset hud :name "draw-hud")
(tset colliders :active false)

(local ui (tiny.processingSystem))
(tset ui :filter (. (require "functions-draw") :ui :filter))
(tset ui :process
      (utils.process (. (require "functions-draw") :ui :process)))
(tset ui :name "draw-ui")
(tset colliders :active false)

(local quips (tiny.processingSystem))
(tset quips :filter (. (require "functions-draw") :quips :filter))
(tset quips :process
      (utils.process (. (require "functions-draw") :quips :process)))
(tset quips :name "draw-quips")
(tset colliders :active false)


{:sprites sprites :tiles tiles :background background :grid grid :colliders colliders
 :foreground foreground :pointer pointer :ai-debug ai-debug :hud hud :ui ui :quips quips}
