(local params (require "params"))
(local gamestate (require "lib.gamestate"))
(local pause {})
(local buttons (require "buttons"))

(local elements
       [{:type :title :y 100 :w 400 :h 60 :text "Welcome Home SunSun"}
        {:type :title :y 250 :w 400 :h 60 :text "<Paused>"}
        {:type :button :y 400 :oy -10 :ox 0 :w 400 :h 70 :text "Continue"}
        {:type :button :y 500 :oy -10 :ox 0 :w 400 :h 70 :text "Controls"}])

(local element-click
       {"Continue"
        (fn [] (gamestate.switch (if blackboard.story (require mode) (require "game"))))
        
        "Controls"
        (fn []
          (gamestate.switch (require "instructions")))})

(fn pause.draw [self]
    (love.graphics.setColor params.colours.background)
    (love.graphics.rectangle "fill" 0 0 960 640) 
    (buttons:draw elements))

(fn pause.update [self dt]
  (buttons:update elements element-click))

(fn pause.enter [self]
  (buttons:enter))

(fn pause.keypressed [self key]
  (when (= key "escape")
    (gamestate.switch (if blackboard.story (require mode) (require "game")))
    ;;(gamestate.switch (require "popup") "day1")
    ))


pause
