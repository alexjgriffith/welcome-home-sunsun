(local gamestate (require "lib.gamestate"))
(local tiny (require "lib.tiny"))

(local params (require "params"))
(local world (require "state"))
(local game {})

(var frames 0)
(var hud true)
(var keydown false)

(fn game.draw [self]
  (local draw-order (if (or (not hud) (not blackboard.story))
                       [:background :tiles :sprites
                           :foreground                           
                           :pointer
                           :quips
                        ]
                                              [:background :tiles :sprites
                           :foreground                           
                            :ui
                           :pointer
                           :quips
                        ]))
  (when (and world.ces.draw.sprites world.ces.draw.background)
    (each [_ draw (ipairs draw-order)]
      ((. world.ces.draw draw :update) (. world.ces.draw draw) nil))))


(fn game.init [self]  
  (when (not world.ces)
    (local draw-systems (require "system-draw"))
    (local save-map-system (. (require "system-editor") :save-map))
  (local level ((require "prefab-map") "house"))
  (local bump-world-prefab ((require "prefab-bump-world") {} level.level))
  (local player ((require "prefab-player") bump-world-prefab))
  (tset world :ces
        (tiny.world 
         (require "prefab-background")
         player
         level
         bump-world-prefab
         ((require "prefab-pointer"))
         (require "prefab-foreground")
         ;;(. (require "system-editor") :editor)
         save-map-system
         ;;(require "system-controler")
         (require "system-sprite-update")
         (require "system-physics")
         (require "system-pointer")
         (require "system-ai-update")
         (require "system-objects")
         (require "system-time")
         (require "system-quips")))
  (tset world :ces :draw {})
  (tset world :ces :temp-pointer nil)
  (tset world :ces :camera {:x 0 :y 0 :l 0 :t 0})
  (each [name system (pairs draw-systems)]
    (tset world :ces :draw name (tiny.addSystem world.ces system)))
  (tset world :ces :save-map save-map-system)
  (tset world :ces :player player)
  (tset world :ces :bump-world bump-world-prefab)
  (tset world :ces :objects level.level.data.objs)
  (gamestate.switch (require "popup") "day1"))
 )

(fn game.enter [self]
  (love.mouse.setVisible false)
  (sound.play :bgm)
  (sound.set-bg-volume 1))

(fn game.leave [self]
  (love.mouse.setVisible true)
  (sound.set-bg-volume 0.15))

(fn game.update [self dt]
  (when world.ces.update
    (world.ces.update world.ces dt))
  (when profile
    (set frames (+ frames 1))
    (when (= 0 (% frames 100))
      (print (love.profiler.report "time" 20))
      (love.profiler.reset))))

(fn game.keyreleased [self key]
   
    ;;(world.ces.save-map.update world.ces.save-map "assets/temp.fnl")
    ;;(world.ces.save-map.update world.ces.save-map "assets/temp.fnl")
    )

(fn game.keypressed [self key]
  (when (= key "escape")
    (gamestate.switch (require "pause")))
   (if (and (= key "tab") (not keydown))
    (do 
      (if hud
          (set hud false)
          (set hud true))
      (set keydown true))
    (set keydown false)
    )

    )


game
