(local params (require "params"))

(local sounds {:bgm {:type :music
                     :pitch 0
                     :vol params.background-music.vol
                     :source (love.audio.newSource params.background-music.value
                                                   "stream")}})


(each [key value (pairs params.sfx)]
  (tset sounds key {:type :sfx :pitch 0.3
                    :vol value.vol
                    :pitch value.pitch
                    :source (love.audio.newSource value.value "static")}))

(local sound-levels {:music 0.1 :sfx 0.5})

;; (: sounds.bgm.source :setLooping true)

(: sounds.bgm.source :setLooping true)

;; (: sounds.click.source :setPitch 0.7)

(fn set-sound-levels []
    (each [key value (pairs sound-levels)]
          (lume.map sounds
                    (fn [x]
                        (when (= x.type key)
                          (: x.source :setVolume (* x.vol value)))))))

(set-sound-levels)

(local click-sounds
       {:index 1 :length 20
        :data []})

(for [i 0 click-sounds.length]
  (table.insert click-sounds.data                
                (let [value params.sfx.click]
                  {:type :sfx :pitch 0.3
                   :vol value.vol
                   :pitch value.pitch
                   :source (love.audio.newSource value.value "static")})))

(local meaw-sounds
       {:index 1 :length 20
        :data []})

(for [i 0 meaw-sounds.length]
  (table.insert meaw-sounds.data                
                (let [value params.sfx.meaw]
                  {:type :sfx :pitch 0.3
                   :vol value.vol
                   :pitch value.pitch
                   :source (love.audio.newSource value.value "static")})))


(fn play [name]
           (when (and (not (: (. sounds name :source) :isPlaying))
                      (not (love.filesystem.getInfo "mute")))
             (if (= :sfx (. sounds name :type))
                 (do
                     (: (. sounds name :source)
                        :setPitch (+ (+ (. sounds name :pitch) 0.9)
                                     (* 0.2 (math.random))))
                     (: (. sounds name :source) :play))
                 (: (. sounds name :source) :play))))

(fn play-obj [obj]  
  (obj.source:setPitch (+ (+ obj.pitch 0.9)
                          (* 0.2 (math.random))))
  (obj.source:setVolume obj.vol)
  (obj.source:play))

{;; :make (fn [name]
 ;;         ;; remove all unused temps
 ;;         (local option (. params.sfx :name)) 
 ;;         (when (and option (: (. option :value))
 ;;                    (not (love.filesystem.getInfo "mute")))
 ;;           (local source (love.audio.newSource option.value))
 ;;           (source:setPitch (+ (+ (. sounds name :pitch) 0.9)
 ;;                               (* 0.2 (math.random))))
 ;;           (source:setVolume option.vol)
 ;;           (table.insert temp-sounds source)
 ;;           (source:play)))
 :click  (fn []
           (tset click-sounds :index (+ 1 click-sounds.index))
           (when (> click-sounds.index click-sounds.length)
             (tset click-sounds :index 1))
           (play-obj (. click-sounds.data click-sounds.index)))
  :meaw  (fn []
           (tset meaw-sounds :index (+ 1 meaw-sounds.index))
           (when (> meaw-sounds.index meaw-sounds.length)
             (tset meaw-sounds :index 1))
           (play-obj (. meaw-sounds.data meaw-sounds.index)))
 :play play
 :set-bg-volume (fn [level] (sounds.bgm.source:setVolume (* sound-levels.music sounds.bgm.vol level)))
 
 :rewind (fn [name]
                 (: (. sounds name :source) :seek 0))
 ;; :update-sound-levels (fn [type level]
 ;;                          (when (and type level)
 ;;                            (tset sound-levels type level))
 ;;                          (set-sound-levels))
 :stop (fn [name] (: (. sounds name :source) :stop))}
