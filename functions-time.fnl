(local gamestate (require "lib.gamestate"))
(local tiny (require "lib.tiny"))
(local params (require "params"))

(local filter (tiny.requireAll "pos" "anim" "controler"))

(fn time-string [int]
  (local floored (math.floor (% int 48)))
  (local hour (math.floor (/ floored 2)))
  (local minute (if (= 0 (% floored 2)) "00" "30"))
  (if (< hour 10)
      (string.format " %d:%s" hour minute)
      (string.format "%d:%s" hour minute)))

(fn process [self player dt]
  (tset player :time blackboard.time)
  (tset player :time (+ player.time (* params.speed dt)))
  (tset blackboard :time player.time)
  ;; (when (> player.time 48)
  ;;   (tset player :time 0)
  ;;   )
  (tset player :time-string (time-string player.time))
  ;; messages
  (tset player :message "Welcome Home Sunsun!")
  (when (not blackboard.story)
    (tset player :message "Sandbox Mode"))
  (when blackboard.story
    (when (< player.stats.hunger.y 0.2)
    (tset player :message "Sunsun is starving!!"))
    (when (< player.stats.hunger.y 0.01)
      (sound.play "on")
    (sound.stop :bgm)
    (gamestate.switch (require "popup") "ending-dead"))
  (when (< player.cash blackboard.cat-food-cost)
    (sound.play "on")
    (sound.stop :bgm)
    (gamestate.switch (require "popup") "ending-broke"))
  (when (> blackboard.call-in-count 3)
    (sound.play "on")
    (sound.stop :bgm)
    (gamestate.switch (require "popup") "ending-fired")))
  )

{:filter filter :process process}
