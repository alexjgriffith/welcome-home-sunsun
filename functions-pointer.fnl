(local tiny (require "lib.tiny"))

(local filter (tiny.requireAll "pointer"))

(local params (require "params"))
(local next-pointer-key params.keys.next-pointer)

(local  within-range-player (. (require "activities") :within-range-player))
(local  within-range-objects (. (require "activities") :within-range-objects))


(fn next-pointer [pointer]
  (let [next-down (love.keyboard.isDown next-pointer-key)]
    (when (and next-down (not pointer.key-down-last-frame))
      (tset pointer :key-down-last-frame true)
      (local number pointer.number)
      (tset number :current (+ number.current 1))
      (when (> number.current number.max)
        (tset number :current 1))
      (tset pointer :type (. pointer.types number.current)))
    (when (not next-down)
      (tset pointer :key-down-last-frame false))))

(fn process [self pointer dt]
  (tset pointer :mouse-down-last-frame blackboard.mouse-down-last-frame)
  (next-pointer pointer)
  (local [hand-key spray-key flail-key] [(love.keyboard.isDown params.keys.hand)
                                         (love.keyboard.isDown params.keys.spray)
                                         (love.keyboard.isDown params.keys.flail)])
  (when flail-key
    (tset pointer.number :current 3)
    (tset pointer :type (. pointer.types 3)))
  (when hand-key
    (tset pointer.number :current 1)
    (tset pointer :type (. pointer.types 1)))
  (when spray-key
    (tset pointer.number :current 2)
    (tset pointer :type (. pointer.types 2)))

  
  (if self.world.temp-pointer
      (do (tset pointer :type self.world.temp-pointer))
      (tset pointer :type (. pointer.types pointer.number.current)))
  ;; check if pointer is over anything return sorted list
  ;; auto select player if player
  (local [x y] [(love.mouse.getPosition)])
  (tset pointer :pos {:x x :y y})
  (local player self.world.player)
  (local objects self.world.objects)
  (local within-range (within-range-player player pointer.pos pointer.radius))
  (local objects-range (within-range-objects objects pointer.pos pointer.radius))
  (when (or within-range (= (# objects-range) 0))
    (tset self.world :temp-pointer nil))
  (let [animation (. pointer.animations pointer.anim)]
    (if (love.mouse.isDown 1)
        (do (tset pointer :anim (.. pointer.type " Loop"))
          (when (not pointer.mouse-down-last-frame)
            (if within-range              
                (tset player :pointer pointer.type)
                (> (# objects-range) 0)
                (tset (. objects-range 1) :clicked true)))
          (animation.update animation dt)
          (tset pointer :mouse-down-last-frame true))
        (love.mouse.isDown 2)
       (do (tset pointer :anim "Spray Loop")
          (when (not pointer.mouse-down-last-frame)
            (if within-range              
                (tset player :pointer "Spray")
                (> (# objects-range) 0)
                (tset (. objects-range 1) :clicked true)))
          (animation.update animation dt)
          (tset pointer :mouse-down-last-frame true))        
        (do (animation.gotoFrame animation 1)
          (tset pointer :anim pointer.type)
          (tset pointer :mouse-down-last-frame false)
          (when (and (> (# objects-range) 0) (not within-range))
            (tset (. objects-range 1) :hovered true))))
    )
  (tset blackboard :mouse-down-last-frame pointer.mouse-down-last-frame)
  )

{:process process :filter filter}
