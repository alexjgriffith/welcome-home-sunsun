{:gravity 20
 :cat-speed  100 ;; 1000 ; 100
 :cat-file {:smuggly "assets/smuggly" :sunsun "assets/Cat Animation"}
 :pointer-file "assets/Pointers"
 :keys {:next-pointer "n" :hand "1" :spray "2" :flail "3"}
 :decay {:boredom 0.15 :hunger 0.08
         :sleepy 0.06 :fear 0}
 :activity-period  2 ;; 0.01 ;; 3
 :bias {:lower-limit 0.05 :upper-limit 20}
 :stair-period 1
 :speed 0.3
 :max-call-ins 3
 :colours {:charcoal [0.21 0.27 0.31 1]
           :cash-green [.522 .733 .396 1]
           :background [1 0.925 0.929 1]
           :light-yellow [0.945 0.918 0.714 1]
           :blood-red [0.737 0.039 0.18]}
 :income 30
 :overtime 10
 :background-music {:value "assets/Pushover.ogg" :vol 1 :pitch 0}
 :sfx {:meaw {:value "assets/meaw.ogg" :vol 1 :pitch 0}
       :on {:value "assets/scratch.ogg" :vol 1 :pitch 0}
       :click {:value "assets/bell_ding4.ogg" :vol 0.5 :pitch 0.5}
       :scratch {:value "assets/bell_ding4.ogg" :vol 1 :pitch 0}
       :hiss {:value "assets/hiss.ogg" :vol 0.2 :pitch 0.2}
       :clank {:value "assets/clank.ogg" :vol 0.15 :pitch 0}
       :cash {:value "assets/cash.ogg" :vol 2 :pitch 0}
       :page {:value "assets/page.ogg" :vol 0.2 :pitch -0.5}}
 }
