(local map (require "map"))
(local subtile (require "subtile"))
(local states (. (require "activities") :states))


(fn pixel-to-tile [px tile-size scale]
  (math.ceil (* scale (/ px tile-size))))

(local map-width 960)
(local map-height 640)

(local tile-size 2)
(local grid-size 8)
(local tile-layers [:ground :for1])
(local object-layers [:objs])
(local all-layers [:clouds :ground :objs :for1 :for2])
(local scale 8)
(local tile-display-width (pixel-to-tile map-width tile-size scale))
(local tile-display-height (pixel-to-tile map-height tile-size scale))

(local tile-sheet (love.graphics.newImage "assets/tiles.png"))

(local sample-grid
       (subtile.newGrid
        grid-size grid-size
        (: tile-sheet :getWidth)
        (: tile-sheet :getHeight)))

(local tile-data
       [{:name :floor  :tile-fun :square10  :x1 1 :y1 1 :x2 3 :y2 1}
       {:name :stairs  :tile-fun :rect  :x 1 :y 4 :w 4 :h 8}
       {:name :stairs-flip  :tile-fun :rect  :x 5 :y 4 :w 4 :h 8}
       {:name :food-empty  :tile-fun :rect  :x 6 :y 1 :w 2 :h 2}
       {:name :food  :tile-fun :rect  :x 8 :y 1 :w 2 :h 2}
       {:name :door  :tile-fun :rect  :x 10 :y 1 :w 4 :h 6}
       {:name :oven  :tile-fun :rect :x 14 :y 1 :w 2 :h 2}
       {:name :fridge  :tile-fun :rect  :x 14 :y 3 :w 2 :h 4}
       {:name :fridge-broken  :tile-fun :rect  :x 16 :y 3 :w 2 :h 4}
       {:name :table  :tile-fun :rect  :x 9 :y 7 :w 4 :h 2}
       {:name :table-broken  :tile-fun :rect  :x 17 :y 7 :w 4 :h 2}
       {:name :bed  :tile-fun :rect  :x 21 :y 3 :w 4 :h 2}
       {:name :bed-broken  :tile-fun :rect  :x 21 :y 1 :w 4 :h 2}
       {:name :couch  :tile-fun :rect  :x 21 :y 7 :w 4 :h 2}
       {:name :couch-broken  :tile-fun :rect  :x 21 :y 5 :w 4 :h 2}
       {:name :shower  :tile-fun :rect  :x 33 :y 1 :w 4 :h 4}
       {:name :shower-broken  :tile-fun :rect  :x 37 :y 1 :w 4 :h 4}
       {:name :dresser  :tile-fun :rect  :x  25 :y 1 :w 4 :h 4}
       {:name :litter  :tile-fun :rect  :x 25 :y 7 :w 2 :h 2}
       {:name :cat-bed  :tile-fun :rect  :x 25 :y 5 :w 2 :h 2}
       {:name :cat-toy  :tile-fun :rect  :x 27 :y 5 :w 2 :h 2}
       {:name :cat-tower  :tile-fun :rect  :x 29 :y 5 :w 2 :h 4}
       {:name :curtain  :tile-fun :rect  :x 35 :y 5 :w 4 :h 4}
       {:name :curtain-broken  :tile-fun :rect  :x 40 :y 5 :w 4 :h 4}
       {:name :uarrow  :tile-fun :rect  :x 13 :y 7 :w 2 :h 2}
       {:name :darrow  :tile-fun :rect  :x 15 :y  7 :w 2 :h 2}])

(fn tile-data-to-tiles [tiles]
  (local return {})
  (each [_ tile (ipairs tiles)]
    (if (= :square10 tile.tile-fun)
        (tset return tile.name (subtile.square10 sample-grid
                                                 tile.x1 tile.y1 tile.x2 tile.y2))
        (= :rect tile.tile-fun)
        (tset return tile.name (subtile.rect sample-grid
                                             tile.x tile.y tile.w tile.h))
        ))  
  return)

(local tiles (tile-data-to-tiles tile-data))

(fn make-brush [tile w h layer]
  (fn [mapin x y]
    (local details (. states tile))    
    (map.add-tile mapin x y layer (map.tile tile :tile w h)
                  (when details (lume.clone details)))))


(fn tile-data-to-brushes [tiles states]
    (local return {})
    (each [_ tile (ipairs tiles)]
      (if (= :square10 tile.tile-fun)
          (tset return tile.name (make-brush tile.name 1 1 :ground))
        (= :rect tile.tile-fun)
        (tset return tile.name (make-brush tile.name tile.w tile.h
                                           (or tile.layer :objs)))
        ))
  return)

(fn update-objects [objects states]
  (each [key object (pairs objects)]
    (local details (. states object.type))
    (when details
       (each [key detail (pairs (lume.clone details))]
         (tset object key (if (= (type detail) "table")
                              (lume.clone detail)
                              detail))))))

(local brushes (tile-data-to-brushes tile-data states))
(local make-shower (. brushes :shower))

(local tileset-batch (love.graphics.newSpriteBatch  tile-sheet 200))

(local object-batch (love.graphics.newSpriteBatch  tile-sheet 100))

(local hover-batch (love.graphics.newSpriteBatch  tile-sheet 10))


(fn load-level [level-file?]  
  (if level-file?
      (let [level (require level-file?)]
        (update-objects level.data.objs states)
        (map.update-tileset-batch tileset-batch tiles level
                                  (* scale tile-size) tile-layers)
        (map.update-tileset-batch object-batch tiles level
                                  (* scale tile-size) object-layers)
        level)
      (map.new tile-display-width tile-display-height tile-layers tile-size)))



(fn [level-file]
  {:tile-size tile-size
   :tile-display-width tile-display-width
   :tile-display-height tile-display-height
   :map-width tile-display-width
   :map-height tile-display-height
   :tileset-batch tileset-batch
   :object-batch object-batch
   :level (load-level level-file)
   :scale scale
   :active true
   :grid-size grid-size
   :object-layers object-layers
   :tile-layers tile-layers
   :all-layers all-layers
   :tiles tiles
   :render true
   :hover-batch hover-batch
   :key-down-last-frame false
   :brush-number 1
   :brushes brushes
   :brush-keys (lume.keys brushes);; [make-curtain make-dresser make-bed make-food make-oven make-fridge
            ;;  make-table make-couch make-litter make-cat-toy make-cat-bed
            ;;  make-cat-tower]
   :brush :shower
 })
