(local tiny (require "lib.tiny"))

(local filter (tiny.requireAll "render" "z" "pos"))

(fn render-sprite [camera entity]
  (camera.draw
   camera
   (fn [l t h w]        
     (let (animation (. entity.animations entity.anim))       
       (animation.draw animation entity.image entity.pos.x entity.pos.y
                       0 5 5)))))

(fn render-tile [camera entity])

(fn render-background [camera entity]
  (camera.draw camera
               (fn [l t h w]
                 (love.graphics.setColor entity.color)
                 (love.graphics.rectangle "fill" 0 0 960 640)
                 (love.graphics.setColor 1 1 1 1))))

(fn compare [self e1 e2]
  (> e1.z e2.z))

(fn process [self entity _dt]
  (local type entity.render.type)
  (local camera self.world.camera.camera)
  (when entity.player
    (camera.setPosition camera (+ entity.pos.x entity.center-offset.x)
                        (+ entity.pos.y entity.center-offset.x)))
  (match type
    "sprite" (render-sprite camera entity)
    "tile" (render-tile camera entity)
    "background" (render-background camera entity)
    _ (error "Render type not defined")))

{:filter filter :compare compare :process process}
