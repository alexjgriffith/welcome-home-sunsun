(local tiny (require "lib.tiny"))
(local filter (tiny.requireAll "pos" "v" "quip"))

(fn process [self quip dt]
  (local world self.world)
  (tset quip.pos :y (+ quip.pos.y (* dt quip.v)))
  (tset quip :current  (+ dt quip.current))
  (when (> quip.current quip.fade)
    (local progress (- quip.time quip.time))
    (local period (- quip.time quip.fade))
    (tset quip :transparency (/ progress period)) ;; change to sigmoid
    )
  (when (> quip.current quip.time)
    (tiny.removeEntity world quip)))

{:filter filter :process process}
