(local params (require "params"))
(local gamestate (require "lib.gamestate"))
(local instructions {})
(local buttons (require "buttons"))

(var from (require "pause"))

(local instruction-text
       "Use the number bar to switch between tools (RMB)\n   1 - hand (for petting)\n   2 - spray (for dissuading)\n   3 - flail (for playing)

For easy access Spay can always be used using the LMB

Hover over an object to see if it can be interactd with
   A food icon indicates that a dish can be filled
   A wrench indicates that the object can be repaired")

(local elements
       [{:type :title :y 50 :w 400 :h 60 :text "Controls"}
        {:type :small-text :y 150 :oy -10 :ox 0 :w 900 :h 70 :text instruction-text}
        {:type :button :y 550 :oy -10 :ox 0 :w 400 :h 70 :text "Return"}])

(local element-click
       {"Return"
        (fn []
          (gamestate.switch from))

        "Credits"
        (fn [])
        
        "Controls"
        (fn [])})

(fn instructions.draw [self]
  (love.graphics.setColor params.colours.background)
  (love.graphics.rectangle "fill" 0 0 960 640) 
  (buttons:draw elements))

(fn instructions.update [self dt]
  (buttons:update elements element-click))


(fn instructions.enter [self lfrom]
  (set from lfrom)
  (buttons:enter))

instructions
