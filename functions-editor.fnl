(local tiny (require "lib.tiny"))
(local map (require "map"))
(local subtile (require "subtile"))


(local editor {})
(local save-map {})

(var click-down false)

(fn erase-brush [mapin x y]  
  (map.remove mapin x y :objs))

(fn on-click [self level brush]
  (local scale level.scale)
  (local grid-size level.grid-size)
  (local camera self.world.camera)
  (local data level.level.data)
  (local tiles level.tiles)
  (local tileset-batch level.tileset-batch)
  (local object-batch level.object-batch)
  (local tile-layers level.tile-layers)
  (local object-layers level.object-layers)
  (local tile-size level.tile-size)
  (local mapin level.level)
  (local update-tileset-batch map.update-tileset-batch)
  (let [[x y]
        (map.xy-to-tile [ (love.mouse.getPosition)]
                        level.scale level.grid-size self.world.camera)]
    (brush mapin x y))
  (each [key tile (pairs (. data :ground))]
    (local [type x y] [tile.type tile.x tile.y])
    (tset data :ground key :index
          (subtile.match (map.get-neighbours type mapin x y :ground))))
  (update-tileset-batch object-batch tiles mapin (* scale tile-size) object-layers)
  (update-tileset-batch tileset-batch tiles mapin (* scale tile-size) tile-layers))

(fn hover [self level dt]
  (local scale level.scale)
  (local grid-size level.grid-size)
  (local camera self.world.camera)  
  (local tiles level.tiles)
  (local hover-batch level.hover-batch)
  (local tile-layers level.all-layers) ;; needs to be all in one sheet
  (local tile-size level.tile-size)
  (local mapin level.level)
  (local update-tileset-batch map.update-tileset-batch)
  (local [x y] (map.xy-to-tile [ (love.mouse.getPosition)] scale grid-size camera))
  (local hover-map (map.new mapin.width mapin.height 1 tile-size))
  (local data hover-map.data)
  (local brush (. level.brushes level.brush))
  (when brush
    (brush hover-map x y)
  (each [key tile (pairs (. hover-map.data :ground))]
    (local [type x y] [tile.type tile.x tile.y])
    (tset data :ground key :index
          (subtile.match  (map.get-neighbours type hover-map x y :ground))))
  (update-tileset-batch hover-batch tiles hover-map (* scale tile-size) tile-layers)))

(fn right-click [self level dt]
  (on-click self level (. level.brushes level.brush)))

(fn left-click [self level dt]
  (on-click self level erase-brush))

(fn write-file [file content]
    (let [f (assert (io.open file "wb"))
          c (: f :write ((require "lib.fennelview") content))]
      (: f :close)
      c))

(tset editor :filter (tiny.requireAll "level" "active"))
(tset save-map :filter (tiny.requireAll "level" "active"))

(fn next-pointer [pointer]
  (local next-pointer-key (. (require "params") :keys :next-pointer))
  (let [next-down (love.keyboard.isDown next-pointer-key)]
    (when (and next-down (not pointer.key-down-last-frame))
      (tset pointer :key-down-last-frame true)
      (tset pointer :brush-number (+ 1 pointer.brush-number))
      (when (> pointer.brush-number (# pointer.brush-keys))
        (tset pointer :brush-number  1))
      (tset pointer :brush (. pointer.brush-keys pointer.brush-number)))
    (when (not next-down)
      (tset pointer :key-down-last-frame false))))

(fn editor.process [self level dt]
  (next-pointer level)
  (when (love.mouse.isDown 1)    
      (when (not click-down)
          (right-click self level dt)
          (set click-down true))
      (set click-down false))
  (when (love.mouse.isDown 2)    
      (when (not click-down)
          (left-click self level dt)
          (set click-down true))
      (set click-down false))  
  (hover self level dt))

(fn save-map.process [self level filename]
  ;; (assert (= "string" (type-of filename)))
  (write-file filename level.level))

(tset save-map :active false)

{:editor editor
 :save-map save-map}
