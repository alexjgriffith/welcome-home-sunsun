(local params (require "params"))
(local gamestate (require "lib.gamestate"))
(local credits {})
(local buttons (require "buttons"))

(var from (require "pause"))

(local credit-text
       "Music (Pushover) - CyberSDF (CC-BY)
Font (Avara) - (OFL)
Library (Lume) - RXI (MIT/X11)
Library (anim8,bump) - Enrique Cota (MIT)
Engine (LÖVE) - LÖVE Dev Team (Zlib)
Language (Fennel) - Calvin Rose (MIT/X11)")

(local elements
       [{:type :title :y 50 :w 400 :h 60 :text "Credits"}
        {:type :small-text :y 150 :oy -10 :ox 0 :w 900 :h 70 :text credit-text}
        {:type :button :y 550 :oy -10 :ox 0 :w 400 :h 70 :text "Return"}])

(local element-click
       {"Return"
        (fn []
          (gamestate.switch from))
        
        "Controls"
        (fn [])})

(fn credits.draw [self]
  (love.graphics.setColor params.colours.background)
  (love.graphics.rectangle "fill" 0 0 960 640) 
  (buttons:draw elements))

(fn credits.update [self dt]
  (buttons:update elements element-click))


(fn credits.enter [self lfrom to]
  (set from to)
  (buttons:enter))

credits
