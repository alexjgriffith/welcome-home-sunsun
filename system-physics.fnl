(local tiny (require "lib.tiny"))
(local utils (require "utils"))

(local physics (tiny.processingSystem))
(tset physics :filter (. (require "functions-physics") :filter))
(tset physics :process
      (utils.process (. (require "functions-physics") :process)))
(tset physics :name "physics")

physics
