(local params (require "params"))
(local gamestate (require "lib.gamestate"))
(local choose-kitty {})
(local buttons (require "buttons"))
(local loader (require "loader"))

(local animations {:sunsun (loader params.cat-file.sunsun true)
                   :smuggly (loader params.cat-file.smuggly true)})

(local sprite-position-x 540)
(local sprite-position-x 540)

(local elements
       [{:type :title :y 100 :w 400 :h 60 :text "Choose Kitty"}
        
        {:type :animated-button :y 350 :oy -20 :ox 0 :w 400 :h 90 :text "Sunsun"
         :x sprite-position-x
         :anim "Sit Sleep"
         :hovered "Stand Talk"
         :not-hovered "Sit Sleep"
         :flippedH false
         :image animations.sunsun.image
         :animations animations.sunsun.animations
         :grid animations.sunsun.grid
         :param animations.sunsun.param}
        
        {:type :animated-button :y 470 :oy -20 :ox 0 :w 400 :h 90 :text "Smuggly"
         :x sprite-position-x
         :image animations.smuggly.image
         :anim "Sit Sleep"
         :hovered "Stand Talk"
         :not-hovered "Sit Sleep"
         :flippedH false
         :animations animations.smuggly.animations
         :grid animations.smuggly.grid
         :param animations.smuggly.param}])

(local element-click
       {"Smuggly"
        (fn []
          (sound.play "on")
          (tset blackboard :cat-type "assets/smuggly")
          (tset blackboard :cat-name "Smuggly")
          (tset blackboard :cat-pronoun-1 "him")
          (tset blackboard :cat-pronoun-2 "he")
          (tset blackboard :cat-pronoun-3 "He")
          (gamestate.switch (require mode)))
        
        "Sunsun"
        (fn []
          (sound.play "on")
          (tset blackboard :cat-type "assets/Cat Animation")
          (tset blackboard :cat-name "Sunsun")
          (tset blackboard :cat-pronoun-1 "her")
          (tset blackboard :cat-pronoun-2 "she")
          (tset blackboard :cat-pronoun-3 "She")
          (gamestate.switch (require mode)))})

(fn choose-kitty.draw [self]
  (love.graphics.setColor params.colours.background)
  (love.graphics.rectangle "fill" 0 0 960 640) 
  (buttons:draw elements))

(fn choose-kitty.update [self dt]
  (buttons:update elements element-click dt))


(fn choose-kitty.enter [self]
  (buttons:enter))

choose-kitty
