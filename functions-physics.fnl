(local tiny (require "lib.tiny"))
(local params (require "params"))

(local physics {})
(tset physics :filter (tiny.requireAll "bump-world-reference" "vel"))

(fn physics.process [self entity dt]
  (local bump-world self.world.bump-world)
  (local gravity (if (< 0 entity.vel.y) (/ params.gravity 2) params.gravity))
  (local vel (math.min 100 (+ entity.vel.y (* gravity dt))))
  (local y (+ vel entity.pos.y))
  (local x entity.pos.x)
  (local col entity.collision)
  (local [actual-x actual-y cols len] [(bump-world.world.move
                                        bump-world.world
                                        entity.bump-world-reference
                                        (+ x col.ox) (+ y col.oy))])
  (tset entity :bump-world-reference :x actual-x)
  (tset entity :bump-world-reference :y actual-y)
  (if (= len 0)
      (tset entity :vel :y vel)
      (do ;; (pp cols)
       (tset entity :vel :y 0)))
  (tset entity :pos :y (- actual-y col.oy))
  (tset entity :pos :x (- actual-x col.ox))
  (when entity.tello
    (bump-world.world:update entity.bump-world-reference
                             entity.tello.x entity.tello.y)
    (tset entity :pos :x entity.tello.x)
    (tset entity :pos :y entity.tello.y)
    (tset entity :tello nil)
    )
)

physics
