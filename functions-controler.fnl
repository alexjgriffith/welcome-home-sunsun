(local tiny (require "lib.tiny"))
(local filter (tiny.requireAll "pos" "anim" "controler"))

(fn process [self player dt]
  (local [l r spc] [(love.keyboard.isDown "a")
                    (love.keyboard.isDown "d")
                    (love.keyboard.isDown "space")])
  (when spc    
    (set player.vel.y -4)
    ;; (pp player.vel)
    (set player.z player.pos.y))
  (if (and l (not r))
      (do
          (tset player :anim "Walk")
        (tset player :pos :flipped false)        
        (set player.pos.x (- player.pos.x (* dt player.speed ))))
      (and r (not l))
      (do
          (tset player :anim "Walk")
        (tset player :pos :flipped true)
        (set player.pos.x (+ player.pos.x (* dt player.speed))))
      (tset player :anim "Stand Idle")))

{:filter filter :process process}
