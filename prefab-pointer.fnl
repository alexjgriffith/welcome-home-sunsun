(local params (require "params"))
(local loader (require "loader"))

(local pointer-animations (loader params.pointer-file  true))
(local pointer {:pointer true
                :key-down-last-frame false
                :mouse-down-last-frame false
                :pos {:x 0 :y 0}
                :radius 20
                :render true :type "Hand"
                :types ["Hand" "Spray" "Flail"] :number {:current 1 :max 3} })



(each [key value (pairs pointer-animations)]
  (tset pointer key value))
(tset pointer :anim "Hand")

(fn []
  (love.mouse.setVisible false)
  pointer)
