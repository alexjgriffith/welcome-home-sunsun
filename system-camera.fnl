(local tiny (require "lib.tiny"))

(local camera-functions (require "functions-camera"))

(local camera-system (tiny.processingSystem))
(tset camera-system :filter (. (require "functions-camera") :filter))
(tset camera-system :compare
      (if develop
          (fn [self e1 e2]
            ((. (require "functions-camera") :compare) e1 e2))
          (. (require "functions-camera") :compare)))
(tset camera-system :process
      (if develop
          (fn [self entities dt]
            ((. (require "functions-camera") :process) self entities dt))
          (. (require "functions-camera") :process)))

(tset camera-system :name "camera")
(tset camera-system :active false)
;; (tset camera-system :camera camera)
camera-system
