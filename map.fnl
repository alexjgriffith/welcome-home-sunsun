(local subtile (require "subtile"))
(local map {})


(fn map.update-tileset-batch [batch tiles mapin tileSize layers? offset-x?]
    (: batch :clear)
    (local offset-x (or offset-x? 0))
    (local layers layers?);;(or layers? [:sun :clouds :ground :objs]))
    (let [tsize (/ tileSize 2)]
      (each [_ level (pairs layers)]
            (each [id tile (pairs (. mapin :data level))]
                  (if (= tile.library :subtile)
                      (do
                       (subtile.batch batch tsize  tile.x  tile.y 
                                      (. tiles tile.type) tile.index offset-x))
                      (= tile.library :tile)
                      (subtile.batch-rect batch tsize  tile.x  tile.y 
                                      (. tiles tile.type) tile.w
                                      tile.h mapin.width offset-x)))))
    (: batch :flush))


(fn map.xy-to-tile [[x y] scale tileSize camera]
  [(math.floor (/ (- x camera.l) (* scale tileSize)))
   (math.floor (/ (- y camera.t) (* scale tileSize)))])

(fn map.xyl-to-id [x y l width height]
    (+ (* y width) x))

(fn map.tile [type library ?w ?h]
    (var tile {:library library :type type})
    (when (= library :tile)
      (tset tile :w ?w)
      (tset tile :h ?h))
    tile)

(fn map.new [width height levels tilesize]    
    (let [ret { :sun {} :clouds {} :ground {} :objs {} :for1 {} :for2 {}}]
      {:data ret :width width :height height :id 0 :tilesize tilesize}))

(fn map.add-tile [mapin x y l tile ?details]
    (var index (map.xyl-to-id x y l mapin.width mapin.height))
    (when tile
      (tset tile :x x)
      (tset tile :y y)
      (tset tile :l l)
      (tset tile :id mapin.id)
      (tset map :id (+ mapin.id 1))
      (when ?details
        (each [key value (pairs ?details)]
          (tset tile key value))
        ;;(pp ?details)
        )
      ;;(pp tile)
      )
    (tset mapin.data l index tile)
    mapin)

(fn map.remove [mapin x y l]
    (var index (map.xyl-to-id x y l mapin.width mapin.height))
    (tset mapin.data l index nil)
    mapin)

(fn map.replace [mapin x y l tile ?details]
  (map.remove mapin x y l)
  (map.add-tile mapin x y l tile ?details))

(fn map.get-neighbours [type mapin x y l ?width ?height]
    (let [tiles
          (. mapin.data l)
          neighbour?          
          (fn [i j]
            (var max 0)
            (let [xp (if ?width
                         (if  (= (+ x i ) (+ ?width 0)) 0
                              (= (+ x i ) -1) (- ?width 1)
                              (+ x i) )
                         (+ x i))
                  tile (. tiles
                         (map.xyl-to-id xp  (+ y j) l mapin.width mapin.height))]
              (if tile
                  (if (= type (. tile :type))
                      1
                      0)
                  0)))]
      {:right
       (neighbour? 1 0)
       :left (neighbour? -1 0)
       :up (neighbour? 0 -1)
       :down (neighbour? 0 1)
       :up-right (neighbour? 1 -1)
       :up-left (neighbour? -1 -1)
       :down-right (neighbour? 1 1)
       :down-left (neighbour? -1 1)}))

(fn map.update-neighbours [mapin layer]
    (each [key tile (pairs (. mapin.data layer))]
              (tset mapin :data layer key :index
                    (subtile.match  (map.get-neighbours tile.type mapin tile.x tile.y :for1 mapin.width mapin.height)))))

map
