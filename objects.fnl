(local activities (require "activities"))
(local objects {})



(tset objects :food
      { ;; a multiplier of the weighted precent chance this action will be used
       ;;:states []
       :state :food-empty
       :size {:width 8 :height 8 :ox 4 :oy 8}
       :w 2
       :h 2})

objects
