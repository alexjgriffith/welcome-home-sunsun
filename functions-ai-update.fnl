(local tiny (require "lib.tiny"))
(local params (require "params"))

(local goto (. (require "activities") :goto))
(local near (. (require "activities") :near))
(local update-stat (. (require "activities") :update-stat))
(local use (. (require "activities") :use))
(local activity-reset (. (require "activities") :activity-reset))

(local filter (tiny.requireAll "pos"  "ai" "goal-stack" "stats"))

(local spray-text (lume.split (love.filesystem.read "text/spray.txt") "\n"))
(local hand-text (lume.split (love.filesystem.read "text/hand.txt") "\n"))
(local flail-text (lume.split (love.filesystem.read "text/flail.txt") "\n"))

(fn min-stat [stats]
  (lume.reduce stats (fn [acc v]
                       (if (< v.value.y acc.value.y) v acc))))

(fn make-weighted-option [key index activity]
  {:key key
   :index index
   :activity activity.activity
   :base activity.base
   :bias activity.bias
   :weight (* activity.bias activity.base)})

(fn no-goals [player]
  (player.goal-stack:empty))

(fn message [world message]
  (local [px py] [(love.mouse.getPosition)])
  (world:addEntity ((require "prefab-quip") nil
                    (string.format message)                    
                    {:x  (- px (/ 500 2))
                     :y (- py 64)} {:font :pet
                                    :width 500
                                    :v -200})) )

(fn select-goal [player objects stats?]
  (local hunger player.stats.hunger)
  (local boredom player.stats.boredom)
  (local sleepy player.stats.sleepy)
  (local stats (or stats? [{:name :hunger :value hunger}
                           {:name :boredom :value boredom}
                           {:name :sleepy :value sleepy}]))
  (local activity-type (min-stat stats))
  ;;(local base-options {:hunger [] :boredom [] :sleepy []})
  ;;(local options (or options? base-options))
  (local options [])
  (each [key object (pairs objects)]
    (when object.activities
        (each [index activity (pairs object.activities)]
          (when (= activity.stat activity-type.name)
            (table.insert options
                        (make-weighted-option key index activity))))))
  (match  [(length options) (length stats)]
    [0 1] []
    [0 _] 
    (select-goal player objects
                 (lume.filter stats
                              (fn [stat]
                                (not (= activity-type.name stat.name)))))
    [_ _]
    (do  
        (local max (lume.reduce options
                                (fn [acc v] (+ acc v.weight)) 0))  
      (local select (math.random max))
      (local goal (lume.reduce options
                               (fn [acc v]
                                 (if (and (> select acc.value)
                                          (<= select (+ acc.value v.weight)))
                                     {:value (+ acc.value v.weight)
                                      :goal {:key v.key :index v.index}}
                                     {:value (+ v.weight acc.value)
                                      :goal acc.goal}
                                     ))
                               {:value 0 :goal nil}))
      
      (when (and goal.goal goal.goal.key)
        (local object (. objects goal.goal.key))
        (if (near player object)
            [{:goal (lume.clone goal.goal) :action :use}]
            [{:goal (lume.clone goal.goal) :action :use}
             {:goal (lume.clone goal.goal) :action :goto}]))
      )))

(fn rand-region [low high]
    (+ (* (math.random) (+ high (- low)))  low ))

(fn rand-message [str]  
  (. str (math.floor (rand-region 1 (# str)))))

(fn reactions [self player objects]
  ;;(pp player.pointer)
  ((. {:Flail
       (fn [player objects]
         (player.goal-stack:clear)
         (message self.world  (rand-message flail-text) :flail)
         (sound.rewind "meaw")
         (sound.play "meaw")
         )       
       :Spray
       (fn [player objects]
         (local goal (player.goal-stack:peak))
         (when (and goal goal.goal goal.goal.key goal.goal.index)
           (local action (. objects goal.goal.key :activities goal.goal.index))
           (tset action :bias (math.max params.bias.lower-limit
                                        (* action.bias 0.95))))
         (sound.rewind "hiss")
         (message self.world (rand-message spray-text) :spray)
         (sound.play "hiss")
         (activity-reset)
         (player.goal-stack:clear)
         )
       
       :Hand
       (fn [player objects]
         (local goal (player.goal-stack:peak))
         ;; (pp player.goal-stack)
         (sound.rewind "meaw")
         (sound.play "meaw")
         (message self.world (rand-message hand-text) :hand)         
         (when (and goal goal.goal goal.goal.key goal.goal.index
                    (= :use goal.action))
           (local action (. objects goal.goal.key :activities goal.goal.index))
           ;;(pp (. objects goal.goal.key))           
           (tset action :bias (math.min params.bias.upper-limit (* action.bias 1.05)))
           )
         )}
      player.pointer) player objects))

(fn process [self player dt]
  (set player.stats.hunger.value blackboard.hunger)
  
  (each [key stat (pairs player.stats)]
    (update-stat stat dt))
  (local objects self.world.objects )
  
  (var clear-stack false)
  (each [k goal (pairs player.goal-stack.stack)]
    (when (not (. objects goal.goal.key))
      (set clear-stack true)))
  (when clear-stack
    (player.goal-stack:clear))

  (when player.pointer
    (reactions self player objects)
    (tset player :pointer nil))
  
  (when (no-goals player)
    (local goals (select-goal player objects))
    (when goals
      (each [_ goal (pairs goals)]      
        (player.goal-stack:push goal)))
    ;;(pp player.goal-stack)
    )

  (local goal (player.goal-stack:peak))
  (when (and goal goal.goal)
    ;;(pp (string.format "ai-update.process gaol.name : %s" goal.name))
    (local object (. self.world.objects goal.goal.key))
    ;; (pp object)
    (if (= goal.action "goto")
        (when object (goto player object objects dt))
        (and (= goal.action :use) (near player object))
        (do
            (let [activity (. object :activities goal.goal.index :activity)]
              (when (use activity player object dt) ;; returns true when finished
                (player.goal-stack))))
        ;; (pp goal)
        ))
    ;;(when (near player object)
    ;;(tset object :health 0)))
  (set blackboard.hunger player.stats.hunger.value))

{:filter filter :process process}
