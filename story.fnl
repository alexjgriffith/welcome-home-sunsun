(local gamestate (require "lib.gamestate"))
(local params (require "params"))
(local game (require "game"))
(local story {})

(local state {;;:cash 50 :hunger 5 :time 12 :cat-food 1
              :called-in false
              :call-in-count 0
              :at-home true
              :at-work false
              :awake false
              :sleep true
              :truth true
              :overtime false
              :skipped-work false
              :broken nil
              :up-late false})

(local events {"Day 1 6:00" {:event "day1" :complete false}
               "6:00" {:event "day" :complete false}
               "7:00" {:event "wake-up" :complete false :depends [:sleep]}
               "9:30" {:event "work" :complete false}
               "10:30" {:event "call" :complete false :depends [:called-in]}
               "17:30" {:event "home" :complete false :depends [:at-work]}
               "19:30" {:event "home-force":complete false
                        :depends [:at-work :overtime]}
               "Day 1 20:30" {:event "visit1" :complete false :depends [:at-home]}
               "Day 3 20:30" {:event "visit2" :complete false :depends [:at-home]}
               "Day 5 20:30" {:event "visit3" :complete false :depends [:at-home]}
               "22:00" {:event "sleep" :complete false}
               "0:00" {:event "sleep-again" :complete false :depends [:awake]}
               "2:00" {:event "force-sleep" :complete false :depends [:awake]}})

(fn long-time-string [int]
  (local day (+ 1 (math.floor (/ int 48))))
  (local floored (math.floor (% int 48)))
  (local hour (math.floor (/ floored 2)))
  (local minute (if (= 0 (% floored 2)) "00" "30"))
  (if (< hour 10)
      (string.format "Day %d %d:%s" day hour minute)
      (string.format "Day %d %d:%s" day hour minute)))

(fn short-time-string [int]
  (local day (+ 1 (math.floor (/ int 48))))
  (local floored (math.floor (% int 48)))
  (local hour (math.floor (/ floored 2)))
  (local minute (if (= 0 (% floored 2)) "00" "30"))
  (if (< hour 10)
      (string.format "%d:%s" hour minute)
      (string.format "%d:%s" hour minute)))

(fn passes-depends [event]
  (local depends (or event.depends []))  
  (local ret (lume.reduce (lume.map depends (fn [dep] (. state dep)))
                          (fn [acc v] (and acc v)) true))
  ret)

(fn call-event [event]
  (each [key value (pairs events)]
    (when (not (= value event))
        (tset value :complete false)))
  (when (passes-depends event)
    (gamestate.switch (require "popup") event.event)))

;; this needs to be cleaned up
(fn check-event-at-time [time]
  (local date-str (long-time-string time))
  (local time-str (short-time-string time))
  (if 
    (and (. events date-str)
           (not (. events date-str :complete)))
    (do (tset events date-str :complete true)
      (call-event (. events date-str))
      )
    (and (not (. events date-str))
         (. events time-str)
         (not (. events time-str :complete)))
    (do (tset events time-str :complete true)
      (call-event (. events time-str)))
    (= 3 (math.floor (% time 48)))
    (each [key value (pairs events)]
      (tset value :complete false))))

(fn story.draw [self]  
  (game:draw)
  (when (or state.at-work state.sleep)
    (love.graphics.setColor params.colours.charcoal)
    (love.graphics.rectangle "fill" 0 0 960 640)))

(fn story.update [self dt]  
  ;;(tset state :time (+ state.time (* params.speed dt)))
  (check-event-at-time blackboard.time)
  ;; (when (and state.call-in (or state.truth state.lie))
  ;;   (tset state :call-in-count (+ state.call-in-count 1))
  ;;   (tset state :call-in false))
  (game:update dt))

(fn story.enter [self from params]
  (each [key value (pairs (or params {}))]    
    (if (= "number" (type value))
        (tset blackboard key (+ value (. blackboard key)))
        (tset state key value)))

  ;;(local blackboard {:cash state.cash :time state.time :hunger state.hunger})
  ;;(pp params)
  (game:enter))

(fn story.init [self]
  (game:init))

(fn story.keypressed [self key]
  (game:keypressed key))

(fn story.keyreleased [self key]
  (game:keyreleased key))

story
