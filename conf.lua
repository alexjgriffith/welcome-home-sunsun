love.conf = function(t)
   t.gammacorrect = true
   t.title, t.identity = "SunSun", "SunSun"
   t.modules.joystick = false
   t.modules.physics = false
   t.window.width = 960
   t.window.height = 640
   t.window.vsync = true
   t.window.x = 100
   t.window.y = 100
   t.window.display = 1
   t.version = "11.2"
   t.gammacorrect = true
end
