(local tiny (require "lib.tiny"))
(local controler-system (tiny.processingSystem))

(tset controler-system :filter (. (require "functions-controler") :filter))

(tset controler-system :process
      (if develop
          (fn [self entities dt]            
            ((. (require "functions-controler") :process) self entities dt))
          (. (require "functions-controler") :process)))

(tset controler-system :name "controler")

;; (pp controler-system)
controler-system


