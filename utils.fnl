(fn process [function]
  (if develop
          (fn [self entities dt]
            (function self entities dt))
          function))

(fn compare [function]
   (if develop
          (fn [self e1 e2]
            (function e1 e2))
          function))

(fn filter [function]
  function)

{:process process
 :compare compare
 :filter filter}
