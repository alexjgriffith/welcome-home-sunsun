(local tiny (require "lib.tiny"))
(local anim8 (require "lib.anim8"))
(local sprite-update-system (tiny.processingSystem))

(tset sprite-update-system :filter (tiny.requireAll "animations" "anim" "pos"))

(fn sprite-update-system.process [self player dt]
  (let (animation (. player.animations player.anim))
    (tset player :animations player.anim :flippedH player.pos.flipped)
    (animation.update animation dt)))


sprite-update-system
