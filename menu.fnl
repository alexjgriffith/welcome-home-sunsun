(local params (require "params"))
(local gamestate (require "lib.gamestate"))
(local menu {})
(local buttons (require "buttons"))

(local elements
       [{:type :title :y 100 :w 400 :h 60 :text "Welcome Home SunSun"}
        {:type :button :y 300 :oy -10 :ox 0 :w 400 :h 70 :text "New Game"}
        {:type :button :y 400 :oy -10 :ox 0 :w 400 :h 70 :text "Credits"}
        {:type :button :y 500 :oy -10 :ox 0 :w 400 :h 70 :text "Controls"}])

(local element-click
       {"New Game"
        (fn []                    
          (gamestate.switch (require "choose-kity")))
        
        "Credits"
        (fn []
          (gamestate.switch (require "credits") (require "menu")))
        
        "Controls"
        (fn []
          (gamestate.switch (require "instructions")))})

(fn menu.draw [self]
  (love.graphics.setColor params.colours.background)
  (love.graphics.rectangle "fill" 0 0 960 640) 
  (buttons:draw elements))

(fn menu.update [self dt]
  (buttons:update elements element-click))


(fn menu.enter [self]
  (buttons:enter))

menu
