;; stats -> Hunger Boredom Sleepy
;; secondary-stats -> Fear, Freindly (always decreasing unless fed or played)
;; actions -> eat meaw claw sleep exc-good exc-bad play purr goto run-from
;; all actions hold for at least 3 seconds unless Fear hits a critical threshold
(local params (require "params"))

(fn sigmoid [x xo k l]
  (/ l (+ 1 (math.exp (- (* k ( - x xo)))))))

(fn new-stat [name start rate pdps]
  {:value start
   :name name
   :y (sigmoid start 0 1 1)
   :rate rate
   :pdps pdps}
  )

(fn update-stat [stat dt]
  (tset stat :value (math.max -5 (- stat.value (* dt stat.rate))))
  (tset stat :y (sigmoid stat.value 0 1 1))
  ;; Check if the stat has droped low enough
  (when (< stat.y 0.1)
    ;; 1-(1-p)^N
    ;; Check if entity has died
    (when (> (love.math.random) (- 1 (^ (- 1 stat.pdps) (/ 1 dt))))
      ;;(love.quit)
      ;; (pp (string.format "update-stat %s < %d dead" stat.name 0.1))
      true
      )
    ))

;; assosiated with cat
(local stats {:hunger (new-stat :hunger 0.7 params.decay.hunger 0.1)
              :boredom (new-stat :boredom 1 params.decay.boredom 0)
              :sleepy (new-stat :sleepy 1 params.decay.sleepy 0)
              :fear (new-stat :fear 1 0 0)})


(fn player-pos [player]
  [ (+ player.pos.x 16) (+ player.pos.y 30)])

(fn object-pos [object]
  [(* (+ object.x (/ object.w 4)) 16)
   (* (+ object.y   (/ object.h 2) ) 16)])

(fn pointer-pos [pointer]
  [(- (/ pointer.x  4) 0) (- (/ pointer.y 4) 0)])

(fn near [player object]
  ;; this is an absolute mess, these numbers should be moved back into
  ;; their entities
  (local [px py] (player-pos player))
  (local [ox oy] (object-pos object))
   (< (+ (^ (- px ox) 2) (^ (- py oy) 2)) 1))

(fn euclid-distance-sq [[ax ay] [bx by]]
  (+ (^ (- ax bx) 2) (^ (- ay by) 2)))

(fn within-range-player [player pos radius]
  (local p1 (player-pos player))
  (local p2 (pointer-pos pos))
  (local distance (euclid-distance-sq  p1 p2))
  ;; (pp [p1 p2])
  (> (^ radius 2) distance))

(fn within-range-objects [objects pos radius]
  (local filter
         (fn [object]
           (local object-pos (object-pos object))
           (local b (pointer-pos pos))
           (> (^ radius 2) (euclid-distance-sq object-pos b))))
  (local sort
         (fn [a b]
           (local object-pos-a (object-pos a))
           (local object-pos-b (object-pos b))
           (local pos-norm (pointer-pos pos))
           (< (euclid-distance-sq object-pos-a pos-norm)
              (euclid-distance-sq object-pos-b pos-norm))
           ))
  (lume.sort (lume.filter objects filter) sort))

(fn filter-type [type]
  (fn [_player object]
    (= object.type type)))

(fn floor-filter-player [player objects]
  (local return {}) ;; a list of keys
  (local [px py] (player-pos player))
  (each [key object (pairs objects)]
    (local [ox oy] (object-pos object))
    ;; (pp (string.format "floor-filter-player px:%d oy:%d" py oy))
    (when (<  (^ (- py oy) 2) 1)      
      (table.insert return key)))
  return)

(fn floor-filter-object [object objects]
  ;; this is an absolute mess, these numbers should be moved back into
  ;; their entities
   (local return {}) ;; a list of keys
   (local [px py] (object-pos object))
   (each [key object (pairs objects)]
     (local [ox oy] (object-pos object))
     (when (<  (^ (- py oy) 2) 1)
       (table.insert key return)))
   return)

(fn find-on-current-floor [player objects callback]
  (var return {})
  (each [_ index (ipairs (floor-filter-player player objects))]
    ;; (pp (string.format "find-on-current-floor index:%d" index))
    (when (callback player (. objects index))
      (table.insert return index)))
  return)

(fn find-on-target-floor [object objects callback]
  (var return nil)
  (each [_ index (ipairs (floor-filter-object object objects))]
    (when (callback object (. objects index))
      (table.insert return index)))
  return)

(fn goto [player object objects dt]
  (local [px py] (player-pos player))
  (local [ox oy] (object-pos object))

  (local remaining (math.abs (- px ox)))
  (local distance (* dt player.speed))
  
  (match [(if (< oy py) :upstairs (> oy py) :downstairs :same-floor)
          (if (> ox px) :right (< ox px) :left :reached)]
    [:same-floor :reached]
    (player.goal-stack)
    [:same-floor  :right]
    (do (tset player :anim "Walk")
      (tset player :pos :flipped true)
      
      (set player.pos.x (+ player.pos.x (math.min distance remaining))))
    [:same-floor  :left]
    (do (tset player :anim "Walk")
      (tset player :pos :flipped false)        
      (set player.pos.x (- player.pos.x (math.min distance remaining ))))
    [:same-floor  :reached]
    (tset player :anim "Stand Idle")    
    [:upstairs  _]
    (do
      ;; find stairs on your floor
      (local stairs (find-on-current-floor player objects (filter-type "uarrow")))
      (when (> (length stairs) 0)
        (player.goal-stack {:goal {:key (. stairs 1)  :index 1} :action :use})
        (player.goal-stack {:goal {:key (. stairs 1) :index 1} :action :goto})
        ))
    [:downstairs  _]
    (do
      ;; find stairs on your floor
      (local stairs (find-on-current-floor player objects (filter-type "darrow")))
      (when (> (length stairs) 0)
        (player.goal-stack {:goal {:key (. stairs 1)  :index 1} :action :use})
        (player.goal-stack {:goal {:key (. stairs 1) :index 1} :action :goto})
        ))
    )
  )

(fn default-update [activity player object dt]
  (tset activity :time (+ activity.time dt))
  (if (> activity.time activity.end)
      true
      false))

(fn default-complete [activity player object]
  (tset activity :time 0)
  (tset activity :type nil)
  (tset activity :params {}))

(fn eat-update [activity player object dt]  
  (tset player :anim "Stand Eat")
  (default-update activity player object dt))

(fn eat-complete [activity player object]
  (default-complete activity player object)
  (tset player :stats :hunger
        :value (math.min 5 (+ player.stats.hunger.value 2)))
  (tset object :health (- object.health 1)))

(fn eat-pre [activity player object]
  (sound.play :meaw))

(fn meaw-update [activity player object dt]  
  (tset player :anim "Stand Talk")
  (sound.play :meaw)
  (default-update activity player object dt))

(fn meaw-complete [activity player object]
  (default-complete activity player object))

(fn meaw-pre [activity player object]
  (sound.play :meaw))

(fn scratch-pre [activity player object]  
  (sound.play "hiss"))

(fn scratch-update [activity player object dt]  
  (tset player :anim "Stand Scratch")
  (default-update activity player object dt))

(fn scratch-complete [activity player object]
  (default-complete activity player object)
  (tset player :stats :boredom
        :value (math.min 5 (+ player.stats.boredom.value 2)))
  (when object.health
    (tset object :health (- object.health 1)))
  (sound.play "clank"))

(fn play-update [activity player object dt]  
  (tset player :anim "Stand Scratch")
  (default-update activity player object dt))

(fn play-complete [activity player object]
  (default-complete activity player object)
  (tset player :stats :boredom
        :value (math.min 5 (+ player.stats.boredom.value 3))))

(fn play-pre [activity player object]
  (sound.play :meaw))

(fn purr-update [activity player object dt]  
  (tset player :anim "Sit Idle")
  (default-update activity player object dt))

(fn purr-complete [activity player object]
    (tset player :stats :sleepy
        :value (math.min 5 (+ player.stats.sleepy.value 1)))
  (default-complete activity player object))

(fn purr-pre [activity player object]
  (sound.play :meaw))

(fn sleep-update [activity player object dt]  
  (tset player :anim "Sit Sleep")
  (default-update activity player object dt))

(fn sleep-complete [activity player object]
  (default-complete activity player object)  
  (tset player :stats :sleepy
        :value (math.min 5 (+ player.stats.sleepy.value 2))))

(fn tello-up-1-update [activity player object dt]
  (local anim "Stand Disapear")
  (when (not (= player.anim anim))    
    ((. player.animations anim :gotoFrame)  (. player.animations anim) 1)
    ;;(pp player.animations)
    )
  (tset player :anim anim )
  (default-update activity player object dt))

(fn tello-update [activity player object dt]
  (local anim "Stand Disapear")
  (when (not (= player.anim anim))    
    ((. player.animations anim :gotoFrame)  (. player.animations anim) 1)
    )
  (tset player :anim anim )
  (default-update activity player object dt))

(fn tello-up-2-complete [activity player object]
  (tset player :tello {:x 195 :y 25})
  (default-complete activity player object))

(fn tello-down-2-complete [activity player object]
  (tset player :tello {:x 175 :y 85})
  (default-complete activity player object))

(fn tello-up-1-complete [activity player object]
  (tset player :tello {:x 53 :y 85})
  (default-complete activity player object))

(fn tello-down-1-update [activity player object dt]  
  (local anim "Stand Disapear")
  (when (not (= player.anim anim))
    ((. player.animations anim :gotoFrame) (. player.animations anim) 1))
  (tset player :anim anim )
  (default-update activity player object dt))

(fn tello-down-1-complete [activity player object]
  (tset player :tello {:x 65 :y 120})
  (default-complete activity player object))

(fn tello-pre [activity player object]
  (sound.play :page))

(local activities
       {:type nil
        :time 0
        :end params.activity-period
        :params {}
        :options {:eat {:complete eat-complete :update eat-update :pre eat-pre}
                  :meaw  {:complete meaw-complete :update meaw-update
                          :pre meaw-pre}
                  :claw {:complete scratch-complete :update scratch-update
                         :pre scratch-pre}
                  :sleep {:complete sleep-complete :update sleep-update}
                  :exc-good {:complete eat-complete :update default-update}
                  :exc-bad {:complete eat-complete :update default-update}
                  :play {:complete play-complete :update play-update :pre play-pre}
                  :purr {:complete purr-complete :update purr-update}
                  :tello-up-1 {:complete tello-up-1-complete :update tello-up-1-update :end params.stair-period :pre tello-pre}
                  :tello-down-1 {:complete tello-down-1-complete :update tello-down-1-update :end params.stair-period :pre tello-pre}
                  :tello-up-2 {:complete tello-up-2-complete :update tello-update
                               :end params.stair-period :pre tello-pre}
                  :tello-down-2 {:complete tello-down-2-complete :update tello-update :end params.stair-period :pre tello-pre}
                  }})


;; activity

(fn update [self player object dt]
  (local params self.params)
  (local finished (params.update self player object dt))
  (when finished (params.complete self player object))
  finished)

(fn use [activity player object dt]
  (when (not (= activity activities.type))
    ;; (default-complete activity player object)
    (tset activities :type activity)
    (tset activities :params (. activities.options activity))
    (if activities.params.end
        (tset activities :end activities.params.end)
        (tset activities :end params.activity-period))
    (if activities.params.pre
        (activities.params.pre activity player object))
    )
  (update activities player object  dt))

(fn activity-reset []
  (tset activities :type nil))

;; assosiated with objects
(local
 states
 {
  :food
  {:activities
   [{:stat "hunger" :activity :eat :base 10 :bias 1}
    {:stat "hunger" :activity  :meaw :base 1 :bias 1}]
   :health 1
   :next-state :food-empty}
  
  :food-empty
  {:activities
   [{:stat "hunger" :activity  :meaw :base 10 :bias 1}]
   :hover "Food"
   :cost 1
   :next-state :food}
  
  :curtain
  {:activities
   [{:stat "boredom" :activity :claw :base 10 :bias 1}]
   :health 1
   :next-state :curtain-broken}
  
  :curtain-broken
  {:activities
   [{:stat "boredom" :activity :claw :base 1 :bias 1}]
   :hover "Wrench"
   :cost 1
   :next-state :curtain}
  
  :oven
  {:activities
   [{:stat "sleepy" :activity "sleep" :base 10 :bias 1}]}

  :couch
  {:activities
   [{:stat "boredom" :activity :claw :base 10 :bias 1}
    {:stat "sleepy" :activity :sleep :base 10 :bias 1}
    {:stat "sleepy" :activity :purr :base 5 :bias 1}
    {:stat "hungry" :activity :eat :base 1 :bias 1}
    {:stat "exc" :activity :pee :base 1 :bias 1}]
   :health 5
   :next-state :couch-broken}
  
  :couch-broken
  {:activities
   [{:stat "boredom" :activity :claw :base 1 :bias 1}
    {:stat "hunger" :activity  :meaw :base 1 :bias 1}
    {:stat "sleepy" :activity  :sleep :base 1 :bias 1}]
   :hover "Wrench"
   :cost 20
   :next-state :couch}
  
  :litter
  {:activities
   [{:stat "exc" :activity :exc-good :base 1 :bias 1}]}
  
  :cat-bed
  {:activities
   [{:stat "sleepy" :activity :sleep :base 5 :bias 1}
    {:stat "sleepy" :activity :purr :base 50 :bias 1}]}
  
  :cat-toy
  {:activities
   [{:stat "boredom" :activity :play :base 5 :bias 1}
    {:stat "sleepy" :activity :purr :base 5 :bias 1}]}
  
  :cat-tower
  {:activities
   [{:stat "boredom" :activity :play :base 5 :bias 1}
    {:stat "sleepy" :activity :purr :base 5 :bias 1}
    {:stat "sleepy" :activity :sleep :base 5 :bias 1}]}

  :fridge
  {:activities
   [{:stat "boredom" :activity :claw :base 10 :bias 1}]
   :health 2
   :next-state :fridge-broken}

  :fridge-broken
  {:activities
   [{:stat "boredom" :activity :claw :base 1 :bias 1}]
   :hover "Wrench"   
   :cost 10
   :next-state :fridge}

  :bed
  {:activities
   [{:stat "boredom" :activity :claw :base 10 :bias 1}]
   :health 1
   :next-state :bed-broken}
  
  :bed-broken
  {:activities
   [{:stat "boredom" :activity :claw :base 1 :bias 1}]
   :hover "Wrench"     
   :cost 2
   :next-state :bed}

  :shower
  {:activities
   [{:stat "boredom" :activity :claw :base 10 :bias 1}]
   :health 1
   :next-state :shower-broken}
  
  :shower-broken
  {:activities
   [{:stat "boredom" :activity :claw :base 1 :bias 1}]
   :cost 10
   :hover "Wrench"   
   :next-state :shower}

  :dresser
  {:activities
   [{:stat "boredom" :activity :claw :base 10 :bias 1}]
   :health 3}
  
  :table
  {:activities
   [{:stat "boredom" :activity :claw :base 10 :bias 1}]
   :health 3
   :next-state :table-broken}

  :table-broken
  {:activities
   [{:stat "boredom" :activity :claw :base 1 :bias 1}]
   :cost 10
   :hover "Wrench"   
   :next-state :table}
  })

{:near near
 :goto goto
 :update-stat update-stat
 :stats stats
 :states states
 :use use
 :within-range-player within-range-player
 :within-range-objects within-range-objects
 :activity-reset activity-reset}
