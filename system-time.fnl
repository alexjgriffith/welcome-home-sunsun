(local tiny (require "lib.tiny"))
(local time-system (tiny.processingSystem))

(tset time-system :filter (. (require "functions-time") :filter))

(tset time-system :process
      (if develop
          (fn [self entities dt]            
            ((. (require "functions-time") :process) self entities dt))
          (. (require "functions-time") :process)))

(tset time-system :name "time")

(tset time-system :active true)
;; (pp time-system)
time-system
