(local params (require "params"))

(local buttons {})

(local element-font {:title :title-font :button :normal-font
                     :text :normal-font
                     :small-text :small-font
                     :animated-button :normal-font})

(fn button-get-pos [element]
  (local x (/ 960 2))
  [(- x (+ element.ox (/ element.w 2)))
   (+ element.oy element.y) element.w element.h])


(fn element-draw-default [element]
 (love.graphics.setColor params.colours.charcoal)
 (love.graphics.setFont (. font (. element-font element.type)))
 (love.graphics.printf element.text 0 element.y 960 "center"))

(fn element-draw-textbox [element]
 (love.graphics.setColor params.colours.charcoal)
 (love.graphics.setFont (. font (. element-font element.type)))
 (love.graphics.printf (text-mod element.text)
                       (/ (- 960 element.w) 2) element.y  element.w "left"))

(fn element-draw-button [element]
  (local [x y w h] (button-get-pos element))
  (love.graphics.setColor params.colours.charcoal)
  (love.graphics.setFont (. font (. element-font element.type)))
  (love.graphics.printf element.text x element.y w "center"))

(fn element-draw-animated-button [element]
  (local [x y w h] (button-get-pos element))
  (love.graphics.setColor params.colours.charcoal)
  (love.graphics.setFont (. font (. element-font element.type)))
  (love.graphics.printf element.text (+ 10 x) element.y (- w 20) "left")
  (love.graphics.push)
  (local scale 4)
  (love.graphics.setColor 1 1 1 1)
  (love.graphics.scale scale scale)
  (let [animation (. element.animations element.anim) ]
    (animation.draw animation element.image 
                    (math.floor (/  element.x  scale))
                    (math.floor (/ (- element.y 65) scale))))
  (love.graphics.pop)
  (love.graphics.setColor params.colours.charcoal)
  )


(local elements-draw
       {:title
        (fn [element]
          (element-draw-default element))
        :text
        (fn [element]
          (element-draw-default element))
        :small-text
        (fn [element]
          (element-draw-textbox element))
        
        :animated-button
        (fn [element]
          (local [x y w h] (button-get-pos element))
          (if element.hover
              (do
                  (love.graphics.setColor params.colours.light-yellow)
                (love.graphics.rectangle "fill" x y w h)
                (element-draw-animated-button element)
                (love.graphics.rectangle "line" x y w h))
              (do
                (element-draw-animated-button element)
              (love.graphics.rectangle "line" x y w h))))
        
        :button
        (fn [element]          
          (local [x y w h] (button-get-pos element))          
          (if element.hover
              (do
                  (love.graphics.setColor params.colours.light-yellow)
                (love.graphics.rectangle "fill" x y w h)
                (element-draw-button element)
                (love.graphics.rectangle "line" x y w h))
            (do
                (element-draw-button element)
                (love.graphics.rectangle "line" x y w h))))
        })

(local elements-update
       {:animated-button
        (fn [element dt]
          (tset element :anim
                (if element.hover element.hovered element.not-hovered))
          (let (animation (. element.animations element.anim))
            (tset element :animations element.anim :flippedH element.flipped)
            (animation.update animation dt)))})

(local elements-hover
       {:button
        (fn [element [x y]]
          (sound.click))
        :animated-button
        (fn [element [x y]]
          (sound.meaw))})

(fn update [elements dt]
  (each [_ element (pairs elements)]
    (let [update-fun (. elements-update element.type)]
      (when update-fun
        (update-fun element dt)))))

(fn hover [elements [x y]]
  (each [_ element (pairs elements)]
    (let [hover-fun (. elements-hover element.type)]
      (when hover-fun
        (local [ex ey ew eh] (button-get-pos element))
          (if (and (> x ex) (< x (+ ex ew)) (> y ey) (< y (+ ey eh)))
              (when (not element.hover)
                (tset element :hover true)
                (hover-fun element [x y]))
              (tset element :hover nil))))))

(fn click [elements button [x y] element-click]
  (when (not button)
    (tset blackboard :mouse-down-last-frame false))
  (when (= (. blackboard :mouse-down-last-frame) false)
      (each [_ element (pairs elements)]
        (when (or (= :button element.type) (= :animated-button element.type))
          (local [ex ey ew eh] (button-get-pos element))
          (when (and button (> x ex) (< x (+ ex ew)) (> y ey) (< y (+ ey eh)))
            (tset blackboard :mouse-down-last-frame true)
            ((. element-click element.text)))))))

(fn buttons.draw [self elements]
 (each [_ element (ipairs elements)]
    ((. elements-draw element.type) element)))

(fn buttons.update [self elements element-click dt]
  (local [x y] [(love.mouse.getPosition)])
  (local button (love.mouse.isDown 1))  
  (hover elements [x y])
  (update elements dt)
  (click elements button [x y] element-click))


(fn buttons.enter [self]
  (love.mouse.setVisible true))

buttons
