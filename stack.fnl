(local stack-fns {})

(fn stack-fns.pop [self]
  (table.remove self.stack))

(fn stack-fns.clear [self]
  (tset self :stack {}))

(fn stack-fns.peak [self]
  (. self.stack (# self.stack)))

(fn stack-fns.push [self value]
  (table.insert self.stack value))

(fn stack-fns.length [self]
  (# self.stack))

(fn stack-fns.empty [self]
  (= 0 (# self.stack)))

(fn call [self ?insert]
  (if ?insert
      (stack-fns.push self ?insert)
      (stack-fns.pop self)))

(local stack-mt {:__index stack-fns :__call call})

(fn []
  (setmetatable {:stack {}} stack-mt))
