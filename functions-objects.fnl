(local tiny (require "lib.tiny"))
(local near (. (require "activities") :near))
(local map (require "map"))

(local filter (tiny.requireAll "level"))

(fn hovered [self obj]
  (when obj.hover
    (tset self.world :temp-pointer obj.hover))
  (tset obj :hovered nil))

(fn clicked [world obj player level]
  (when (and obj.cost obj.next-state ;;(>= player.cash obj.cost)
             )
    (tset player :cash (- player.cash obj.cost))
    (local [px py] [(love.mouse.getPosition)])
    (world:addEntity ((require "prefab-quip") "cost"
                      (string.format "-$%d" obj.cost)
                      ;; need to center this on object
                      {:x  (- px 48)
                       :y (- py 64)})) 
    ;;(pp obj.next-state)
    (sound.play "cash")
      ((. level.brushes obj.next-state) level.level obj.x obj.y)
      (map.update-tileset-batch
       level.object-batch
       level.tiles
       level.level
       (* level.scale level.tile-size)
       level.object-layers))
  (tset obj :clicked nil))

(fn process [self level dt]   
  (local player self.world.player)
  (set player.cash blackboard.cash)
  (each [_ obj (pairs level.level.data.objs)]
    (when (= "food-empty" obj.type)
      (tset obj :cost blackboard.cat-food-cost))
    (when obj.hovered
      (hovered self obj))
    (when obj.clicked
      (clicked self.world obj player level))
    (when (and obj.health (< obj.health 1) obj.next-state)
      ;;(pp obj.next-state)
      ((. level.brushes obj.next-state) level.level obj.x obj.y)
      (map.update-tileset-batch
       level.object-batch
       level.tiles
       level.level
       (* level.scale level.tile-size)
       level.object-layers))
    )

  ;;(pp (. level.level.data.objs 30730 :health))
   (set blackboard.cash player.cash)
  )

{:filter filter :process process}
